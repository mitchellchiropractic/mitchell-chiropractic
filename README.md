We are passionate about chiropractic care! We love our clients and we are energetic about restoring health. We pass that energy on to everyone who puts their health in our hands. Literally.

Address: 335 Main Street East, Cambridge, ON N1R 1Y6, Canada

Phone: 519-622-1101

Website: http://www.mitchellchiropractic.ca
